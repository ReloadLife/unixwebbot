	function Run(msg, matches)
		if matches[1] == 'f2a' then
			txt = matches[2] 
			for k,v in pairs(redis:smembers('Groups!')) do
				cli.forwardMessages(v, msg.chat_id_, {[0] = msg.reply_to_message_id_}, 1)
			end
		end
		if matches[1] == 'bc' then
			txt = matches[2] 
			for k,v in pairs(redis:smembers('Groups!')) do
				cli.sendText(v, 0, 0, 1, nil, txt, 1, 'MarkDown')
			end
		end
	end
	return {
		cli = {
			_MSG = {
				'^(f2a)$',
				'^(bc) (.*)$',
			},
	--		Pre = Pre,
			run = Run
		},
		CheckMethod = 'f80', -- Also Can use as 'TeleSeed' ! # If use TeleSeed Input Will be msg = { to = {}, from = {}} :))
	--	Cron = Cron
	}