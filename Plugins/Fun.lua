function string.random(length)
   local str = "";
   for i = 1, length do
      math.random(97, 122)
      str = str..string.char(math.random(97, 122));
   end
   return str;
end

function string:split(sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  self:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end

-- DEPRECATED
function string.trim(s)
  print("string.trim(s) is DEPRECATED use string:trim() instead")
  return s:gsub("^%s*(.-)%s*$", "%1")
end

-- Removes spaces
function string:trim()
  return self:gsub("^%s*(.-)%s*$", "%1")
end


function string:isempty()
  return self == nil or self == ''
end

-- Returns true if the string is blank
function string:isblank()
  self = self:trim()
  return self:isempty()
end

-- DEPRECATED!!!!!
function string.starts(String, Start)
  -- print("string.starts(String, Start) is DEPRECATED use string:starts(text) instead")
  -- uncomment if needed
  return Start == string.sub(String,1,string.len(Start))
end

-- Returns true if String starts with Start
function string:starts(text)
  return text == string.sub(self,1,string.len(text))
end
function unescape_html(str)
  local map = {
    ["lt"]  = "<",
    ["gt"]  = ">",
    ["amp"] = "&",
    ["quot"] = '"',
    ["apos"] = "'"
  }
  new = string.gsub(str, '(&(#?x?)([%d%a]+);)', function(orig, n, s)
    var = map[s] or n == "#" and string.char(s)
    var = var or n == "#x" and string.char(tonumber(s,16))
    var = var or orig
    return var
  end)
  return new
end

function download_to_file(url, file_name)
  -- print to server
  -- print("url to download: "..url)
  -- uncomment if needed
  local respbody = {}
  local options = {
    url = url,
    sink = ltn12.sink.table(respbody),
    redirect = true
  }

  -- nil, code, headers, status
  local response = nil

  if url:starts('https') then
    options.redirect = false
    response = {https.request(options)}
  else
    response = {http.request(options)}
  end

  local code = response[2]
  local headers = response[3]
  local status = response[4]

  if code ~= 200 then return nil end

  file_name = file_name or get_http_file_name(url, headers)

  local file_path = "./"..file_name
  -- print("Saved to: "..file_path)
	-- uncomment if needed
  file = io.open(file_path, "w+")
  file:write(table.concat(respbody))
  file:close()

  return file_path
end

local function run_bash(str)
    local cmd = io.popen(str)
    local result = cmd:read('*all')
    return result
end
local json = require('cjson')
--------------------------------
local api_key = nil
local base_api = "https://maps.googleapis.com/maps/api"
--------------------------------
local function get_latlong(area)
	local api      = base_api .. "/geocode/json?"
	local parameters = "address=".. (URL.escape(area) or "")
	if api_key ~= nil then
		parameters = parameters .. "&key="..api_key
	end
	local res, code = https.request(api..parameters)
	if code ~=200 then return nil  end
	local data = json.decode(res)
	if (data.status == "ZERO_RESULTS") then
		return nil
	end
	if (data.status == "OK") then
		lat  = data.results[1].geometry.location.lat
		lng  = data.results[1].geometry.location.lng
		acc  = data.results[1].geometry.location_type
		types= data.results[1].types
		return lat,lng,acc,types
	end
end
--------------------------------
local function get_staticmap(area)
	local api        = base_api .. "/staticmap?"
	local lat,lng,acc,types = get_latlong(area)
	local scale = types[1]
	if scale == "locality" then
		zoom=8
	elseif scale == "country" then 
		zoom=4
	else 
		zoom = 13 
	end
	local parameters =
		"size=600x300" ..
		"&zoom="  .. zoom ..
		"&center=" .. URL.escape(area) ..
		"&markers=color:red"..URL.escape("|"..area)
	if api_key ~= nil and api_key ~= "" then
		parameters = parameters .. "&key="..api_key
	end
	return lat, lng, api..parameters
end
--------------------------------
local function get_weather(location)
	print("Finding weather in ", location)
	local BASE_URL = "http://api.openweathermap.org/data/2.5/weather"
	local url = BASE_URL
	url = url..'?q='..location..'&APPID=eedbc05ba060c787ab0614cad1f2e12b'
	url = url..'&units=metric'
	local b, c, h = http.request(url)
	if c ~= 200 then return nil end
	local weather = json.decode(b)
	local city = weather.name
	local country = weather.sys.country
	local temp = 'دمای شهر '..city..' هم اکنون '..weather.main.temp..' درجه سانتی گراد می باشد\n____________________\n'
	local conditions = 'شرایط فعلی آب و هوا : '
	if weather.weather[1].main == 'Clear' then
		conditions = conditions .. 'آفتابی☀'
	elseif weather.weather[1].main == 'Clouds' then
		conditions = conditions .. 'ابری ☁☁'
	elseif weather.weather[1].main == 'Rain' then
		conditions = conditions .. 'بارانی ☔'
	elseif weather.weather[1].main == 'Thunderstorm' then
		conditions = conditions .. 'طوفانی ☔☔☔☔'
	elseif weather.weather[1].main == 'Mist' then
		conditions = conditions .. 'مه 💨'
	end
	return temp .. '\n' .. conditions
end
--------------------------------
local function calc(exp)
	url = 'http://api.mathjs.org/v1/'
	url = url..'?expr='..URL.escape(exp)
	b,c = http.request(url)
	text = nil
	if c == 200 then
    text = 'Result = '..b..'\n____________________'..msg_caption
	elseif c == 400 then
		text = b
	else
		text = 'Unexpected error\n'
		..'Is api.mathjs.org up?'
	end
	return text
end
--------------------------------
function exi_file(path, suffix)
    local files = {}
    local pth = tostring(path)
	local psv = tostring(suffix)
    for k, v in pairs(scandir(pth)) do
        if (v:match('.'..psv..'$')) then
            table.insert(files, v)
        end
    end
    return files
end
--------------------------------
function file_exi(name, path, suffix)
	local fname = tostring(name)
	local pth = tostring(path)
	local psv = tostring(suffix)
    for k,v in pairs(exi_file(pth, psv)) do
        if fname == v then
            return true
        end
    end
    return false
end
--------------------------------
function Run(msg, matches) 
	msg_caption = ""
	tcpath = ""
	if #matches > 0 then
		if (matches[1]:lower() == 'calc' or matches[1]:lower() == "ماشین حساب" )and matches[2] then 
			return calc(matches[2])
		end
	--------------------------------
		if (matches[1]:lower() == 'tophoto' or matches[1]:lower() == 'عکسش کن') and msg.reply_to_message_id_ then
			function tophoto(arg, data)
				function tophoto_cb(arg,data)
					if data.content_.sticker_ then
						local file = data.content_.sticker_.sticker_.path_
						local file_id = data.content_.sticker_.sticker_.id_
						cli.downloadFile(file_id)
						if file then
							os.rename(file, file:gsub("webp","jpg"))
							cli.sendPhoto(msg.chat_id_, 0, 0, 1, nil, file:gsub("webp","jpg"), msg_caption, dl_cb, nil)
						else
							cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, '_This sticker does not exist. Send sticker again._', 1, 'MarkDown')
						end
					else
						cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, '_This is not a sticker._', 1, 'MarkDown')
					end
				end
				tdcli_function ({ ID = 'GetMessage', chat_id_ = msg.chat_id_, message_id_ = data.id_ }, tophoto_cb, nil)
			end
			tdcli_function ({ ID = 'GetMessage', chat_id_ = msg.chat_id_, message_id_ = msg.reply_to_message_id_ }, tophoto, nil)
		end
	--------------------------------
		if matches[1] == "ping" or matches[1] == "Ping" or matches[1] == "ربات" then
local text = {
"کچلم کردی نزن آنلاینم",
"صدا زدنتو دوس دارم😝✌️🎈",
"انلاینم باو نزززززن",
"😎لیون بوت همیشه آنلاین️😎",
"دیدی آنلاینم😃✌",
"😶گوش به فرمانم قربان😶",
"😭نزن دیگه آنلاینم😫",
"😝آنلاینم عزیز😛",
"دیوث انلاینم😐✌️🏿",
"دیدی زیر آبی نمیرم🙊🎈🥀",
"جونم عشقم😍🎈🥀",
"جووون تو فقط حرف بزن🥀🎈😁",
"جون عمت نزن انلاینم😁",
"درحال انجام وظیفم😕",
}
local random = text[math.random(#text)]
return random
end
		if matches[1]:lower() == 'hack' and (isSudo (msg.sender_user_id_)) then
			if matches[2] then 
				USER = matches[2] 
				cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, "1010111100000101010101010101010101010101010101010101010111100000101010101010101010101010101010101010101010101010101010101011110000010101010101010101010101010101010101010101010101010101010101111000001010101010101010101010101010101010101010101010101010101010111100000101010101010101010101010101010101010101010101010101010101011110000010101010101010101010101010101010101010101010101010101010101111000001010101010101010101010101010101010101010101010101010101010101010101010111110001011011110001110010101", 1, 'MarkDown')
				cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, "در حال هک کردن کاربر ...", 1, 'MarkDown')
				cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, "کاربر ".. USER .." هک شد!", 1, 'MarkDown')
			elseif msg.reply_to_message_id_ ~= 0 then 
				cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, "1010111100000101010101010101010101010101010101010101010111100000101010101010101010101010101010101010101010101010101010101011110000010101010101010101010101010101010101010101010101010101010101111000001010101010101010101010101010101010101010101010101010101010111100000101010101010101010101010101010101010101010101010101010101011110000010101010101010101010101010101010101010101010101010101010101111000001010101010101010101010101010101010101010101010101010101010101010101010111110001011011110001110010101", 1, 'MarkDown')
				cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, "در حال هک کردن کاربر ...", 1, 'MarkDown')
				cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, "کاربر ".."" .." هک شد!", 1, 'MarkDown')
			end
		end
		if matches[1]:lower() == 'help' then
			return [[
*/id* `[Mention|Username|UserID|Reply]`
> نمایش آیدی
*/clean* `[msg|members|all|bots|mutelist|filterlist|deleted]`
> حذف [msg|members|all|bots|mutelist|filterlist|deleted]
*/calc* `[num]` 
> ماشین حساب
*/tophoto* `[reply]`
> تبدیل به عکس
*/tosticker* `[reply]`
> تبدیل به استیکر
*/ping*
> چک کردن آنلاین بودن ربات
*/weather* `[city]`
> آب و هوای شهر 
*/time* 
> دریافت زمان
*/voice* `[text]`
> تبدیل متن به صدا
*/short* `[url]`
> کوتاه کننده لینک
*/tr* `[text]`
> ترجمه متن
*/photo* `[text]`
> ساخت عکس
*/sticker* `[text]`
> ساخت استیکر
*/promote* `[Mention|Username|UserID|Reply]`
> ارتقاع
*/demote* `[Mention|Username|UserID|Reply]`
> تنزل
*/mods* `[list|clean]`
> نمایش لیست مدیران / حدف لیست مدیران
*/muteuser* `[Mention|Username|UserID|Reply]`
> سکوت کاربر
*/unmuteuser* `[Mention|Username|UserID|Reply]`
> حذف سکوت کاربر
*/mutes* `[list|clean]`
> نمایش لیست سکوت / حدف لیست سکوت
*/invite* `[Mention|Username|UserID|Reply]`
> دعوت کاربر
*/kick* `[Mention|Username|UserID|Reply]`
> اخراج 
*/set* `[wlc|link|lang|expire]` <needle>
> تنظیم {متن خوشامد|لینک|زبان|تاریخ انقظا} گروه
*/filter* `[word]`
> فیلتر کلمه
*/unfilter* `[word]`
> حذف فیلتر کلمه
*/[un]banall* `[Mention|Username|UserID|Reply]`
> بن / حذف بن از همه جا
*/banalllist*
> نمایش لیست بن جهانی
*/muteall*
> سکوت همه
*/unmuteall*
> حذف سکوت همه
*/lock|/unlock* `['link', 'mention', 'atsign', 'hashtag', 'persion', 'english', 'edit', 'photo', 'video', 'contact', 'music', 'voice', 'location', 'animation', 'sticker', 'game', 'inline', 'keyboard', 'document', 'media', 'webpage', 'tgservice']`
> قفل/یازکردن 
*/settings*
> تنظیمات
*stats*
> وضعیت
همه دستور ها بدون [/!#] نیز کار میکنند!

			]]
		end
		if (matches[1]:lower() == 'tosticker' or matches[1]:lower() == 'استیکرش کن')  and  msg.reply_to_message_id_ then
			function tosticker(arg, data)
				function tosticker_cb(arg,data)
					if data.content_.ID == 'MessagePhoto' then
						VarDump(data)
						file_id = data.content_.photo_.sizes_[#data.content_.photo_.sizes_ - 1].photo_.id_
						file = data.content_.photo_.sizes_[#data.content_.photo_.sizes_ - 1].photo_.path_
						cli.downloadFile(file_id)
						if file then
							os.rename(file, file:gsub("jpg", "webp"))
							cli.sendDocument(msg.chat_id_, 0, 0, 1, nil, file:gsub("jpg", "webp"), msg_caption, dl_cb, nil)
						else
							cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, '_This photo does not exist. Send photo again._', 1, 'MarkDown')
						end
					else
						cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, "_This is not a photo._", 1, 'MarkDown')
					end
				end
				tdcli_function ({ ID = 'GetMessage', chat_id_ = msg.chat_id_, message_id_ = data.id_ }, tosticker_cb, nil)
			end
			tdcli_function ({ ID = 'GetMessage', chat_id_ = msg.chat_id_, message_id_ = msg.reply_to_message_id_ }, tosticker, nil)
		end
	--------------------------------
		if (matches[1]:lower() == 'weather' or matches[1]:lower() == 'اب وهوا') then
			city = matches[2]
			local wtext = get_weather(city)
			if not wtext then
				wtext = 'مکان وارد شده صحیح نیست'
			end
			return wtext
		end
	--------------------------------
		if (matches[1]:lower() == 'time' or matches[1]:lower() == 'زمان') then
			local url , res = http.request('http://irapi.ir/time/')
			if res ~= 200 then return "No connection" end
			local jdat = json.decode(url)
			return '*Ir Time:* _'..jdat.FAtime..'_\n*Ir Data:* _'..jdat.FAdate..'_\n------------\n*En Time:* _'..jdat.ENtime..'_\n *En Data:* _'..jdat.ENdate.. '_\n'

		end
	--------------------------------
		if (matches[1]:lower() == 'voice' or matches[1]:lower() == 'صدا') then
		local text = matches[2]
		textc = text:gsub(' ','.')
		local url = "http://tts.baidu.com/text2audio?lan=en&ie=UTF-8&text="..textc
		local file = download_to_file(url,'f80.mp3')
		cli.sendDocument(msg.chat_id_, 0, 0, 1, nil, file, msg_caption, dl_cb, nil)
	end

	--------------------------------
		if (matches[1]:lower() == 'tr' or matches[1]:lower() == 'ترجمه') then 
			url = https.request('https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20160119T111342Z.fd6bf13b3590838f.6ce9d8cca4672f0ed24f649c1b502789c9f4687a&format=plain&lang='..URL.escape(matches[2])..'&text='..URL.escape(matches[3]))
			data = json:decode(url)
			return 'زبان : '..data.lang..'\nترجمه : '..data.text[1]..'\n____________________'..msg_caption
		end
	--------------------------------
		if (matches[1]:lower() == 'short' or matches[1]:lower() == 'کوتاه') then
			if matches[2]:match("[Hh][Tt][Tt][Pp][Ss]://") then
				shortlink = matches[2]
			elseif not matches[2]:match("[Hh][Tt][Tt][Pp][Ss]://") then
				shortlink = "https://"..matches[2]
			end
			local yon = http.request('http://api.yon.ir/?url='..URL.escape(shortlink))
			local jdat = json:decode(yon)
			local bitly = https.request('https://api-ssl.bitly.com/v3/shorten?access_token=f2d0b4eabb524aaaf22fbc51ca620ae0fa16753d&longUrl='..URL.escape(shortlink))
			local data = json:decode(bitly)
			local u2s = http.request('http://u2s.ir/?api=1&return_text=1&url='..URL.escape(shortlink))
			local llink = http.request('http://llink.ir/yourls-api.php?signature=a13360d6d8&action=shorturl&url='..URL.escape(shortlink)..'&format=simple')
			local text = ' 🌐لینک اصلی :\n'..check_markdown(data.data.long_url)..'\n\nلینکهای کوتاه شده با 6 سایت کوتاه ساز لینک : \n》کوتاه شده با bitly :\n___________________________\n'..(check_markdown(data.data.url) or '---')..'\n___________________________\n》کوتاه شده با u2s :\n'..(check_markdown(u2s) or '---')..'\n___________________________\n》کوتاه شده با llink : \n'..(check_markdown(llink) or '---')..'\n___________________________\n》لینک کوتاه شده با yon : \nyon.ir/'..(check_markdown(jdat.output) or '---')..'\n____________________'..msg_caption
			return text
		end
	--------------------------------
		if (matches[1]:lower() == 'sticker' or matches[1]:lower() == 'استیکر') then
			local eq = URL.escape(matches[2])
			local w = "500"
			local h = "500"
			local txtsize = "100"
			local txtclr = "ff2e4357"
			if matches[3] then 
				txtclr = matches[3]
			end
			if matches[4] then 
				txtsize = matches[4]
			end
			if matches[5] and matches[6] then 
				w = matches[5]
				h = matches[6]
			end
			local url = "https://assets.imgix.net/examples/clouds.jpg?blur=150&w="..w.."&h="..h.."&fit=crop&txt="..eq.."&txtsize="..txtsize.."&txtclr="..txtclr.."&txtalign=middle,center&txtfont=Futura%20Condensed%20Medium&mono=ff6598cc"
			local file = download_to_file(url,'text.webp')
			cli.sendDocument(msg.chat_id_, 0, 0, 1, nil, file, msg_caption, dl_cb, nil)
		end
	--------------------------------
		if (matches[1]:lower() == 'photo' or matches[1]:lower() == 'عکس') then
			local eq = URL.escape(matches[2])
			local w = "500"
			local h = "500"
			local txtsize = "100"
			local txtclr = "ff2e4357"
			if matches[3] then 
				txtclr = matches[3]
			end
			if matches[4] then 
				txtsize = matches[4]
			end
			if matches[5] and matches[6] then 
				w = matches[5]
				h = matches[6]
			end
			local url = "https://assets.imgix.net/examples/clouds.jpg?blur=150&w="..w.."&h="..h.."&fit=crop&txt="..eq.."&txtsize="..txtsize.."&txtclr="..txtclr.."&txtalign=middle,center&txtfont=Futura%20Condensed%20Medium&mono=ff6598cc"
			local  file = download_to_file(url,'text.jpg')
			cli.sendPhoto(msg.chat_id_, 0, 0, 1, nil, file, msg_caption, dl_cb, nil)
		end
	end
end
--------------------------------
    return {
		cli = {
			_MSG = {
				"^([Hh]elpfun)$",
                "^([Ww]eather) (.*)$",
                "^(اب و هوه) (.*)$",
                "^([Cc]alc) (.*)$",
                "^(ماشین حساب) (.*)$",
                "^([Tt]ime)$",
                "^(زمان)$",
                "^([Pp]ing)$",
                "^([Tt]ophoto)$",
                "^(عکسش کن)$",
                "^([Tt]osticker)$",
                "^(استیکرش کن)$",
                "^([Vv]oice) +(.*)$",
                "^(ویس) +(.*)$",
                "^([Pp]raytime) (.*)$",
                "^([Pp]raytime)$",
                "^([Tt]r) ([^%s]+) (.*)$",
                "^(ترجمه) ([^%s]+) (.*)$",
                "^([Ss]hort) (.*)$",
                "^(کوتاه) (.*)$",
                "^([Pp]hoto) (.+)$",
                "^(عکس) (.+)$",
                "^([Ss]ticker) (.+)$",
                "^(استیکر) (.+)$",
                "^([Hh]ack)$",
                "^([Hh]elp)$",
                "^([Hh]ack) (.*)$",
             	"^(ربات)$",
                "^[!#/]([Hh]ack)$",
                "^[!#/]([Hh]ack) (.*)$",

                "^[#!/]([Tt]ime)$",
                "^[#!/]([Pp]ing)$",
                "^[#!/]([Tt]ophoto)$",
                "^[#!/]([Tt]osticker)$",
                "^[!/#]([Vv]oice) +(.*)$",
                "^[/!#]([Pp]raytime) (.*)$",
                "^[/!#]([Pp]raytime)$",
                "^[!#/]([Tt]r) ([^%s]+) (.*)$",
                "^[!#/]([Ss]hort) (.*)$",
                "^[!#/]([Pp]hoto) (.+)$",
                "^[!#/]([Ss]ticker) (.+)$",
                "^[#!/](ربات)$",
			}, 
			run = Run
		},
		CheckMethod = 'f80',
	}
