		function GpLang(chat_ID)
            if chat_ID then
                return (redis:get(chat_ID..'Lang') or 'FA')
            end
        end
	function Run(msg, matches)
		if #matches > 0 then
			if  (matches[1]:lower() == 'id' or matches[1]:lower() == 'ایدی' or matches[1]:lower() == 'آیدی') then
				if #matches < 2 then
					if tonumber(msg.reply_to_message_id_) == 0 then
						if msg.USER.user_.profile_photo_ then
							if msg.USER.user_.profile_photo_.big_.path_ then
								local Photo = msg.USER.user_.profile_photo_.big_.path_ 
                                USERMSGS = (redis:get(msg.sender_user_id_.."usermsgs") or 0)
								local TEXT = Language(msg.chat_id_, "ID_CAPTION"):format(
									tostring(msg.sender_user_id_),
									tostring(msg.chat_id_),
									tostring(msg.USER.user_.first_name_),
									tostring(msg.USER.user_.username_ or ""),
									tostring(USERMSGS or "")
								)
								cli.sendPhoto(msg.chat_id_, msg.id_, 0, 1, nil, Photo, TEXT)
							else
								cli.downloadFile(msg.USER.user_.profile_photo_.big_.id_)
                                USERMSGS = (redis:get(msg.sender_user_id_.."usermsgs") or 0)
								local TEXT = Language(msg.chat_id_, "ID_BEFOR_DL"):format(
									tostring(msg.sender_user_id_),
									tostring(msg.chat_id_),
									tostring(msg.USER.user_.first_name_),
									tostring(msg.USER.user_.username_ or ""),
									tostring(USERMSGS or "")
								)
								cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
							end
						else
                        USERMSGS = (redis:get(msg.sender_user_id_.."usermsgs") or 0)
							local TEXT = Language(msg.chat_id_, "ID_NOTFOUND"):format(
									tostring(msg.sender_user_id_),
									tostring(msg.chat_id_),
									tostring(msg.USER.user_.first_name_),
									tostring(msg.USER.user_.username_ or ""),
									tostring(USERMSGS or "")
								)
							cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
						end
					else
						cli.getMessage(msg.chat_id_, msg.reply_to_message_id_,
						function (Arg, Data)
							cli.getUserFull(Data.sender_user_id_, function (A, D)
                            USERMSGS = (redis:get(Data.sender_user_id_.."usermsgs") or 0)
								if D.user_.profile_photo_ then
									if D.user_.profile_photo_.big_.path_ then
										local Photo = D.user_.profile_photo_.big_.path_ 
										local TEXT = Language(msg.chat_id_, "ID_CAPTION"):format(
											tostring(D.user_.id_),
											tostring(msg.chat_id_),
											tostring(D.user_.first_name_),
											tostring(D.user_.username_ or ""),
									tostring(USERMSGS or "")
										)
										cli.sendPhoto(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, Photo, TEXT)
									else
										cli.downloadFile(D.user_.profile_photo_.big_.id_)
										local TEXT = Language(msg.chat_id_, "ID_BEFOR_DL"):format(
											tostring(Data.user_.id_),
											tostring(msg.chat_id_),
											tostring(D.user_.first_name_),
											tostring(D.user_.username_ or ""),
									        tostring(USERMSGS or "")
										)
										cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, TEXT, 1, 'MarkDown')
									end
								else
									local TEXT = Language(msg.chat_id_, "ID_NOTFOUND"):format(
											tostring(Data.user_.id_),
											tostring(msg.chat_id_),
											tostring(D.user_.first_name_),
											tostring(D.user_.username_ or ""),
									        tostring(USERMSGS or "")
										)
									cli.sendText(msg.chat_id_, msg.reply_to_message_id_, 0, 1, nil, TEXT, 1, 'MarkDown')
								end
							end, nil)
						end, nil)
					end
				end
				if #matches > 1 then
					if #msg.content_.entities_ > 1 then
						if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
							X = tonumber(msg.content_.entities_[0].user_id_)
						else
							X = nil
						end
					else
						if tonumber(matches[2]) ~= nil then
							X = tonumber(matches[2])
						else
							X = nil
						end
					end
					if type(X) == 'number' then
						cli.getUserFull(
							X,
						function (A, D)
							if D.ID == 'Error' then
								local URL = 'https://id.pwrtelegram.xyz/db/getusername?id='..tostring(X)
								local JDATA = JSON.decode(io.popen("curl "..URL):read("*all"))
								VarDump(JDATA)
								if JDATA.result then
						cli.searchPublicChat(JDATA.result,
							function (A, D)
							print("PWR USERD !!")
								if D.ID == 'Error' then

								end
                                USERMSGS = (redis:get(D.id_.."usermsgs") or 0)
								if D.type_.ID == 'PrivateChatInfo' then 
									if D.photo_ then
										if D.photo_.big_.path_ then
											local Photo = D.photo_.big_.path_
										local TEXT = Language(msg.chat_id_, "ID_CAPTION"):format(
											tostring(D.id_),
											tostring(msg.chat_id_),
											tostring(D.title_),
											tostring(JDATA.result or ""),
									tostring(USERMSGS or "")
										)
											cli.sendPhoto(msg.chat_id_, msg.id_, 0, 1, nil, Photo, TEXT)
										else
											cli.downloadFile(D.photo_.big_.id_)
											local TEXT = Language(msg.chat_id_, "ID_BEFOR_DL"):format(
												tostring(D.id_),
												tostring(msg.chat_id_),
												tostring(D.title_),
												tostring(JDATA.result or ""),
									tostring(USERMSGS or "")
											)
											cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
										end
									else
										local TEXT = Language(msg.chat_id_, "ID_NOTFOUND"):format(
											tostring(D.id_),
											tostring(msg.chat_id_),
											tostring(D.title_),
											tostring(JDATA.result or ""),
									tostring(USERMSGS or "")
										)
										cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
									end
								else
									local TEXT = Language(msg.chat_id_, "ID_CHANNEL"):format(
											tostring(D.id_),
											tostring(msg.chat_id_),
											tostring(D.title_),
											tostring(JDATA.result or ""),
									tostring(USERMSGS or "")
										)
									cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
								end
							end,
						nil)
								else
									local TEXT = Language(msg.chat_id_, "ID_NOTFOUND_USERID"):format(
										tostring(X)
									)
									return TEXT
								end
							else
								if D.user_.profile_photo_ then
									if D.user_.profile_photo_.big_.path_ then
										local Photo = D.user_.profile_photo_.big_.path_ 
										local TEXT = Language(msg.chat_id_, "ID_CAPTION"):format(
											tostring(D.user_.id_),
											tostring(msg.chat_id_),
											tostring(D.user_.first_name_),
											tostring(D.user_.username_ or ""),
									tostring(USERMSGS or "")
										)
										cli.sendPhoto(msg.chat_id_, msg.id_, 0, 1, nil, Photo, TEXT)
									else
										cli.downloadFile(D.user_.profile_photo_.big_.id_)
										local TEXT = Language(msg.chat_id_, "ID_BEFOR_DL"):format(
											tostring(D.user_.id_),
											tostring(msg.chat_id_),
											tostring(D.user_.first_name_),
											tostring(D.user_.username_ or ""),
									tostring(USERMSGS or "")
										)
										cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
									end
								else
									local TEXT = Language(msg.chat_id_, "ID_NOTFOUND"):format(
											tostring(D.user_.id_),
											tostring(msg.chat_id_),
											tostring(D.user_.first_name_),
											tostring(D.user_.username_ or ""),
									tostring(USERMSGS or "")
										)
									cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
								end
							end
						end, nil)
					else
						cli.searchPublicChat(matches[2]:gsub('@', ''),
							function (A, D)

								if D.ID == 'Error' then
									local TEXT = Language(msg.chat_id_, "ID_NOTFOUND_USERNAME"):format(
											tostring(matches[2]:gsub('@', '') or "")
									)
									return TEXT
								end
                                USERMSGS = (redis:get(D.user_.id_.."usermsgs") or 0)
								if D.type_.ID == 'PrivateChatInfo' then 
									if D.photo_ then
										if D.photo_.big_.path_ then
											local Photo = D.photo_.big_.path_
										local TEXT = Language(msg.chat_id_, "ID_CAPTION"):format(
											tostring(D.id_),
											tostring(msg.chat_id_),
											tostring(D.title_),
											tostring(matches[2]:gsub('@', '') or ""),
									tostring(USERMSGS or "")
										)
											cli.sendPhoto(msg.chat_id_, msg.id_, 0, 1, nil, Photo, TEXT)
										else
											cli.downloadFile(D.photo_.big_.id_)
											local TEXT = Language(msg.chat_id_, "ID_BEFOR_DL"):format(
												tostring(D.id_),
												tostring(msg.chat_id_),
												tostring(D.title_),
												tostring(matches[2]:gsub('@', '') or ""),
									tostring(USERMSGS or "")
											)
											cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
										end
									else
										local TEXT = Language(msg.chat_id_, "ID_NOTFOUND"):format(
											tostring(D.id_),
											tostring(msg.chat_id_),
											tostring(D.title_),
											tostring(matches[2]:gsub('@', '') or ""),
									tostring(USERMSGS or "")
										)
										cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
									end
								else
									local TEXT = Language(msg.chat_id_, "ID_CHANNEL"):format(
											tostring(D.id_),
											tostring(msg.chat_id_),
											tostring(D.title_),
											tostring(matches[2]:gsub('@', '') or ""),
									tostring(USERMSGS or "")
										)
									cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, TEXT, 1, 'MarkDown')
								end
							end,
						nil)
					end
				end
			end
		end
	end
	return {
		cli = {
			_MSG = {
				--Patterns :)
				'^([Ii][Dd])$',
				'^(ایدی) (.*)$',
				'^(ایدی)$',
				'^(آیدی)$',
				'^(آیدی) (.*)$',
				'^[/!#]([Ii][Dd])$',
				'^([Ii][Dd]) (.*)$',
				'^[/!#]([Ii][Dd]) (.*)$',
			},
			Pre = Pre,
			run = Run
		},
		CheckMethod = 'f80', -- Also Can use as 'TeleSeed' ! # If use TeleSeed Input Will be msg = { to = {}, from = {}} :))
	--	Cron = Cron
	}