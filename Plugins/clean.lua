		function GpLang(chat_ID)
            if chat_ID then
                return (redis:hget(chat_ID, 'Lang') or 'FA')
            end
        end

    function DelMsgs(ex, result )
    	for k,v in pairs(result.messages_) do
   	 	   	cli.deleteMessages(v.chat_id_, {[0] = v.id_})
  		end
    end
    function call_cb(extra, result)
		for v,k in pairs(result.members_) do
			cli.deleteMessagesFromUser(extra.chat_id, k.user_id_)
		end
	end
	function Run(msg, matches)
		if #matches > 0 then
			if (matches[1]:lower() == 'clean' or matches[1]:lower() == "حذف") and isMod(msg.sender_user_id_, msg.chat_id_) then
				if (matches[2]:lower() == 'پیام' or matches[2]:lower() == "msg") then
						tdcli_function ({
  							  ID = "GetChatHistory",
  							  chat_id_ = msg.chat_id_,
  							  from_message_id_ = msg.id_,
  							  offset_ = 0,
  							  limit_ = (matches[3] or 100)
  						}, DelMsgs, nil)
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return (matches[3] or 100)..'تعداد پیام مورد نظر حذف شدند !'
					else
						return (matches[3] or 100)..'Messages Cleaned !'
					end
				elseif (matches[2]:lower() == 'همه' or matches[2]:lower() == "all") then
				if redis:get(msg.chat_id_.."MHDOOD") then 
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return "بعد از  " .. math.floor(redis:ttl(msg.chat_id_.."MHDOOD") /60/60) .. " ساعت دوباره تلاش کنید !"
					else
						return "Retry after " .. math.floor(redis:ttl(msg.chat_id_.."MHDOOD") /60/60) .. " Hours"
					end
				else 
				redis:setex(msg.chat_id_.."MHDOOD", (60 * 60 * 24), true)
					tdcli_function ({
  						  ID = "GetChannelMembers",
  						  channel_id_ = msg.chat_id_:gsub('-100',''),
  						  filter_ = {
  						    ID = "ChannelMembersKicked"
  						  },
  						  offset_ = 0,
  						  limit_ = 10000
  					}, call_cb, {chat_id=msg.chat_id_})
	  				tdcli_function ({
  						  ID = "GetChannelMembers",
  						  channel_id_ = msg.chat_id_:gsub('-100',''),
  						  filter_ = {
  						    ID = "ChannelMembersRecent"
  						  },
  						  offset_ = 0,
  						  limit_ = 200
  					}, call_cb, {chat_id=msg.chat_id_})
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return 'تمامی پیام هایی که به آنها دسترسی داشتم حذف شد !'
					else
						return 'Messages Cleaned Success !'
					end
					end
				elseif (matches[2]:lower() == 'bots' or matches[2]:lower() == "ربات") then
					tdcli_function ({
  						  ID = "GetChannelMembers",
  						  channel_id_ = msg.chat_id_:gsub('-100',''),
  						  filter_ = {
  						    ID = "ChannelMembersBots"
  						  },
  						  offset_ = 0,
  						  limit_ = 10000
  					}, function (A,D)
  						for v,k in pairs(D.members_) do
							cli.changeChatMemberStatus(msg.chat_id_, k.user_id_, 'Kicked')
						end
  					end, {chat_id=msg.chat_id_})
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return 'تمامی ربات های API اخراج شدند !'
					else
						return 'All Bots Kicked Success !'
					end
				elseif (matches[2]:lower() == 'members' or matches[2]:lower() == "ممبر") then
					tdcli_function ({
  						  ID = "GetChannelMembers",
  						  channel_id_ = msg.chat_id_:gsub('-100',''),
  						  filter_ = {
  						    ID = "ChannelMembersRecent"
  						  },
  						  offset_ = 0,
  						  limit_ = 10000
  					}, function (A,D)
  						for v,k in pairs(D.members_) do
  							if not isMod(k.user_id_, msg.chat_id_) then
								cli.changeChatMemberStatus(msg.chat_id_, k.user_id_, 'Kicked')
							end
						end
  					end, {chat_id=msg.chat_id_})
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return 'تمامی اعضا اخراج شدند !'
					else
						return 'All Members Kicked Success !'
					end
				elseif (matches[2]:lower() == 'blocked' or matches[2]:lower() == "بلاک لیست") then
					tdcli_function ({
  						  ID = "GetChannelMembers",
  						  channel_id_ = msg.chat_id_:gsub('-100',''),
  						  filter_ = {
  						    ID = "ChannelMembersKicked"
  						  },
  						  offset_ = 0,
  						  limit_ = 10000
  					}, function (A,D)
  						for v,k in pairs(D.members_) do
							cli.changeChatMemberStatus(msg.chat_id_, k.user_id_, 'Left')
						end
  					end, {chat_id=msg.chat_id_})
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return '*کاربران بلاک شده گروه ازاد شدند *!'
					else
						return '*Block List Cleaned *!'
					end
				elseif (matches[2]:lower() == 'deleted' or matches[2]:lower() == "حذف شده ها") then
					tdcli_function ({
  						  ID = "GetChannelMembers",
  						  channel_id_ = msg.chat_id_:gsub('-100',''),
  						  filter_ = {
  						    ID = "ChannelMembersRecent"
  						  },
  						  offset_ = 0,
  						  limit_ = 10000
  					}, function (A,D)
  						for v,k in pairs(D.members_) do
  							cli.getUserFull(v.user_id_, 
							function (Arg, data)
  								if data.user_.type_.ID == "UserTypeDeleted"  then
  									cli.changeChatMemberStatus(Arg.chat_id, k.user_id_, 'kicked')
  								end
  							end,{chat_id = msg.chat_id_})
						end
  					end, {chat_id=msg.chat_id_})
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return '*کاربران دیلیت شده اخراج شدند *!'
					else
						return '*Deleted Account Cleaned *!'
					end
				elseif (matches[2]:lower() == 'filterlist' or matches[2]:lower() == "فیلترلیست") then
					redis:del("Filterlist"..msg.chat_id_)
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return '*لیست فیلتر خالی شد*!'
					else
						return '*FilterList Cleaned*!'
					end
				elseif (matches[2]:lower() == 'میوت لیست' or matches[2]:lower() == "mutelist") then
					redis:del("Mutelist"..msg.chat_id_)
					if GpLang(msg.chat_id_):lower() == 'fa' then
						return '*لیست سکوت خالی شد*!'
					else
						return '*mutelist Cleaned*!'
					end
				end
			end
			if matches[1]:lower() == 'addblocklist' and isMod (msg.sender_user_id_, msg.chat_id_) then
				tdcli_function ({
						ID = "GetChannelMembers",
						channel_id_ = msg.chat_id_:gsub('-100',''),
						filter_ = {
						ID = "ChannelMembersKicked"
						},
						offset_ = 0,
						limit_ = 10000
				}, function (A,D)
					for v,k in pairs(D.members_) do
						cli.addChatMember(msg.chat_id_, k.user_id_, 50)
					end
				end, {chat_id=msg.chat_id_})
				if GpLang(msg.chat_id_):lower() == 'fa' then
					return '*کاربران بلاک شده گروه ازاد شدند *!'
				else
					return '*Block List Cleaned *!'
				end
			end
		end
	end

	return {
		cli = {
			_MSG = {
				--Patterns :)
				'^([Cc]lean) ([Mm]sg) (.*)$',
				'^([Cc]lean) ([Aa]ll)$',
				'^([Cc]lean) ([Bb]ots)$',
				'^([Cc]lean) ([Mm]embers)$',
				'^([Cc]lean) ([Bb]locked)$',
				'^([Cc]lean) ([Mm]utelist)$',
				'^([Cc]lean) ([Ff]ilterlist)$',
				'^([Cc]lean) ([Dd]eleted)$',
				'^(حذف) (.*)$',
				'^(حذف) (%S+) (.*)$',
				'^([Aa]ddblocklist)$',
				'^[!#/]([Aa]ddblocklist)$',
				'^[!#/]([Cc]lean) ([Mm]sg) (.*)$',
				'^[!#/]([Cc]lean) ([Aa]ll)$',
				'^[!#/]([Cc]lean) ([Mm]utelist)$',
				'^[!#/]([Cc]lean) ([Ff]ilterlist)$',
				'^[!#/]([Cc]lean) ([Bb]ots)$',
				'^[/!#]([Cc]lean) ([Mm]embers)$',
				'^[/!#]([Cc]lean) ([Bb]locked)$',
				'^[/!#]([Cc]lean) ([Dd]eleted)$',
			},
	--		Pre = Pre,
			run = Run
		},
		CheckMethod = 'f80', -- Also Can use as 'TeleSeed' ! # If use TeleSeed Input Will be msg = { to = {}, from = {}} :))
	--	Cron = Cron
	}