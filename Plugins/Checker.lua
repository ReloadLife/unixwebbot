utf = require 'lua-utf8'
	function DoMessage(msg)
		if msg then
			chat_id = msg.chat_id_ 
			user_id = msg.sender_user_id_
			Group = redis:hgetall(chat_id)
			msg_id = msg.id_
			TEXT = (msg.content_.text_ or msg.content_.caption_ or ' ')
			if not isMod(user_id, chat_id) then
				for k,v in pairs(redis:smembers('Filterlist'..chat_id)) do
					if TEXT:find(v) then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if redis:sismember('Mutelist'..chat_id, user_id) then
					if not isMod(user_id, chat_id) then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.MuteAll == 'true' then
					cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
				end
				if Group.Link == 'DEL' then
					if TEXT:lower():match('t.me/') or TEXT:lower():match('telegram.me/') or TEXT:lower():match('telegram.dog/') or TEXT:lower():match('telegra.ph/') then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.Atsign == 'DEL' then
					if TEXT:match('@')  then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.Hashtag == 'DEL' then
					if TEXT:lower():match('#')  then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.English == 'DEL' then
					if TEXT:lower():match('[a-z]')  then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.Persion == 'DEL' then
					if TEXT:lower():match('[\216-\219][\128-\191]')  then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.Edit == 'DEL' then
					if msg.edit_date_ ~= 0 then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Photo == 'DEL' then
					if msg.content_.ID == 'MessagePhoto' or msg.content_.photo_ then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Video == 'DEL' then
					if msg.content_.ID == 'MessageVideo' then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.ShareNumber == 'DEL' then
					if msg.content_.ID == "MessageContact" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Music == 'DEL' then
					if msg.content_.ID == "MessageAudio" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Voice == 'DEL' then
					if msg.content_.ID == "MessageVoice" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Location == 'DEL' then
					if msg.content_.ID == "MessageLocation" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Animation == 'DEL' then
					if msg.content_.ID == "MessageAnimation" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Sticker == 'DEL' then
					if msg.content_.ID == "MessageSticker" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Game == 'DEL' then
					if msg.content_.ID == "MessageGame" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Inline == 'DEL' then
					if msg.via_bot_user_id_ ~= 0 then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Keyboard == 'DEL' then
					if msg.reply_markup_ then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.File == 'DEL' then
					if msg.content_.ID == "MessageDocument" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Media == 'DEL' then
					if msg.content_.ID ~= "MessageText" then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Webpage == 'DEL' then
					if msg.content_.web_page_ then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				-----------------------------------------
				if Group.Forward == 'DEL' then
					if msg.forward_info_ then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.LongCharr == 'DEL' then
					if utf.len(TEXT) > (tonumber(Group.LongCharrC) or 500) then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
				if Group.ShortCharr == 'DEL' then
					if utf.len(TEXT) < (tonumber(Group.ShortCharrC) or 2) then
						cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
					end
				end
			end
		end
	end
	return {
		cli = {
			_MSG = {
			},
			Pre = DoMessage,
	--		run = Run
		},
		CheckMethod = 'f80', -- Also Can use as 'TeleSeed' ! # If use TeleSeed Input Will be msg = { to = {}, from = {}} :))
	--	Cron = Cron
	}