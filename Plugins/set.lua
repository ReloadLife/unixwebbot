
		function Run(msg, matches)
		if #matches > 0 then
			if  ( matches[1] =='set' or matches[1] == "تنظیم") and isMod(msg.sender_user_id_, msg.chat_id_) then
				if ( matches[2] =='link' or matches[2] == 'لینک') then
					redis:hset(msg.chat_id_, 'GroupLink', matches[3])
					return Language(msg.chat_id_, "LINK_SET")..MarkScape(matches[3])..''
				end
				if ( matches[2] =='wlc' or matches[1] == "خوشامد") then
					redis:hset(msg.chat_id_, 'Welcome', matches[3])
					return Language(msg.chat_id_, "WLC_SET")..MarkScape(matches[3])..''
				end
				if ( matches[2] =='flood' or matches[2] == "رگباری") then
					redis:hset(msg.chat_id_, 'FloodC', matches[3])
					return "تنظیم شد!"
				end
				if ( matches[2] =='floodtime' or matches[2] == "زمان رگباری") then
					redis:hset(msg.chat_id_, 'FloodT', matches[3])
					return "تنظیم شد!"
				end
				if ( matches[2] =='lang' or matches[2] == 'زبان') then
					if matches[3] == ('en' or matches[2] == 'اینگلیسی') then
						redis:hset(msg.chat_id_, 'Lang', 'en')
						return Language(msg.chat_id_, "LANG_SET_EN")
					else
						redis:hset(msg.chat_id_, 'Lang', 'fa')
						return Language(msg.chat_id_, "LANG_SET_FA")
					end
				end
				if ( matches[2] =="expire" or matches[2] == 'انقضا') and isFull(msg.sender_user_id_) then 
					Time = tonumber(os.time()) + 60 * 60 * 24 * tonumber(matches[3] or 30)
					redis:hset(msg.chat_id_, "ExpireTime", Time)
					return "گروه تا " .. os.date("%X %x", Time) .. " شارژ شد !"
				end
			end
			if  ( matches[1] =='filter' or matches[1] == "فیلتر") and isMod(msg.sender_user_id_, msg.chat_id_) then
				redis:sadd('Filterlist'..msg.chat_id_, matches[2])
				return MarkScape(matches[2]) .. Language(msg.chat_id_, "FILTER_PLUS")
			end
			if  ( matches[1] =='filterlist clean' or matches[1] == "حذف فیلتر لیست") and isMod(msg.sender_user_id_, msg.chat_id_) then
				redis:del('Filterlist'..msg.chat_id_)
				return MarkScape(matches[2]) .. Language(msg.chat_id_, "FILTER_CLEAN")
			end
			if  ( matches[1] =='filterlist' or matches[1] == "فیلتر لیست") and isMod(msg.sender_user_id_, msg.chat_id_) then
				if redis:scard('Filterlist'..msg.chat_id_) == 0 then
					return "لیست فیلتر خالیست!"
				else
					local Mutelist = "لیست فیلتر : " .. (redis:hget(msg.chat_id_, 'Title') or msg.chat_id_).. '\n'
					for k, v in pairs(redis:smembers('Filterlist'..msg.chat_id_)) do
						Mutelist = Mutelist.. k ..') '..getUserInfo(v) .. '\n'
					end
					return Mutelist
				end
				return TEXT
			end
			if  ( matches[1] =='unfilter' or matches[2] == 'رفع فیلتر') and isMod(msg.sender_user_id_, msg.chat_id_) then
				redis:srem('Filterlist'..msg.chat_id_, matches[2])
				return MarkScape(matches[2]) .. Language(msg.chat_id_, "FILTER_EGUL")
			end
			if  ( matches[1] =='muteall' or matches[2] == 'سکوت همه') and isMod(msg.sender_user_id_, msg.chat_id_) then
				if not matches[2] or matches[2] == '' then
					redis:hset(''..msg.chat_id_, 'MuteAll', 99999999999999)
					return Language(msg.chat_id_, "MUTE_ALL")
				elseif matches[2] then
					redis:hset(''..msg.chat_id_, 'MuteAll', tonumber(matches[2]))
					return Language(msg.chat_id_, "MUTE_ALL")
				end
			end
			if  ( matches[1] =='unmuteall' or matches[2] == 'رفع سکوت همه') and isMod(msg.sender_user_id_, msg.chat_id_) then
				redis:hset(msg.chat_id_, 'MuteAll', 0)
				return Language(msg.chat_id_, "MUTE_UNALL")
			end
			if matches[1] == 'banalllist' then
				TEXT = ""
				for k,v in pairs(redis:smembers("BlockList")) do 
					TEXT = TEXT .. k .. " ) ".. MarkScape(getUserInfo(v)) .. "\n"
				end
				if redis:scard("BlockList") == 0 then 
					TEXT = "خالی است !"
				end
				return TEXT
			end

			if matches[1]:lower() == 'banall' and isSudo(msg.sender_user_id_) then
				if #matches < 2 then
					if tonumber(msg.reply_to_message_id_) ~= 0 then
						cli.getMessage(msg.chat_id_, msg.reply_to_message_id_,
						function (Arg, Data)
							redis:sadd("BlockList", Data.sender_user_id_)
							cli.sendText(Data.chat_id_, Data.id_, 0, 1, nil,  "بلاک شد ", 1, 'MarkDown')
						end, nil)
					end
				end
				if #matches > 1 then
					if #msg.content_.entities_ then
						if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
							X = tonumber(msg.content_.entities_[0].user_id_)
						else
							X = false
						end
					else
						if tonumber(matches[2]) ~= nil then
							X = tonumber(matches[2])
						else
							X = false
						end
					end
					if X then
						cli.getUserFull( X, function (A, D)
							if D.ID == 'Error' then
								cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil,  "یافت نشد", 1, 'MarkDown')
							end
							redis:sadd("BlockList", D.user_.id_)
							cli.sendText(Data.chat_id_, Data.id_, 0, 1, nil,  "بلاک شد ", 1, 'MarkDown')
						end, nil)
					else
						cli.searchPublicChat(matches[2]:gsub('@', ''), function (A, D)
							if D.ID == 'Error' then
								cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil,  "یافت نشد", 1, 'MarkDown')
							end
							redis:sadd("BlockList", D.id_)
							cli.sendText(Data.chat_id_, Data.id_, 0, 1, nil,  "بلاک شد ", 1, 'MarkDown')
						end, nil)
					end
				end
			end

			if matches[1]:lower() == 'unbanall' and isSudo(msg.sender_user_id_) then
				if #matches < 2 then
					if tonumber(msg.reply_to_message_id_) ~= 0 then
						cli.getMessage(msg.chat_id_, msg.reply_to_message_id_,
						function (Arg, Data)
							redis:srem("BlockList", Data.sender_user_id_)
							cli.sendText(Data.chat_id_, Data.id_, 0, 1, nil,  "انبلاک شد ", 1, 'MarkDown')
						end, nil)
					end
				end
				if #matches > 1 then
					if #msg.content_.entities_ then
						if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
							X = tonumber(msg.content_.entities_[0].user_id_)
						else
							X = false
						end
					else
						if tonumber(matches[2]) ~= nil then
							X = tonumber(matches[2])
						else
							X = false
						end
					end
					if X then
						cli.getUserFull( X, function (A, D)
							if D.ID == 'Error' then
								cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil,  "یافت نشد", 1, 'MarkDown')
							end
							redis:srem("BlockList", D.user_.id_)
							cli.sendText(Data.chat_id_, Data.id_, 0, 1, nil,  "انبلاک شد ", 1, 'MarkDown')
						end, nil)
					else
						cli.searchPublicChat(matches[2]:gsub('@', ''), function (A, D)
							if D.ID == 'Error' then
								cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil,  "یافت نشد", 1, 'MarkDown')
							end
							redis:srem("BlockList", D.id_)
							cli.sendText(Data.chat_id_, Data.id_, 0, 1, nil,  "انبلاک شد ", 1, 'MarkDown')
						end, nil)
					end
				end
			end
		end
		end
	return {
		cli = {
			_MSG = {
			'^([Ss]et) (link) (.*)$',
			'^([Ss]et) (flood) (.*)$',
			'^[/!#]([Ss]et) (flood) (.*)$',
			'^[/!#]([Ss]et) (floodtime) (.*)$',
			'^([Ss]et) (floodtime) (.*)$',
			'^(تنظیم) (لینک) (.*)$',
			'^(تنظیم) (خوشامد) (.*)$',
			'^(تنظیم) (زبان) (.*)$',
			'^(تنظیم) (انقضا) (.*)$',
			'^(تنظیم) (رگباری) (.*)$',
			'^(تنظیم) (زمان رگباری) (.*)$',
			'^([Ss]et) (wlc) (.*)$',
			'^([Ss]et) (lang) (.*)$',
			'^([Ss]et) (expire) (.*)$',
			'^[/!#]([Ss]et) (expire) (.*)$',
			'^[/!#]([Ss]et) (link) (.*)$',
			'^[/!#]([Ss]et) (wlc) (.*)$',
			'^[/!#]([Ss]et) (lang) (.*)$',
			'^([Ff]ilter) (.*)$',
			'^(فیلتر) (.*)$',
			'^[/!#]([Ff]ilter) (.*)$',
			'^([Uu]n[Ff]ilter) (.*)$',
			'^(رفع فیلتر) (.*)$',
			'^[/!#]([Uu]n[Ff]ilter) (.*)$',
			'^[/!#]([Ff]ilterlist)$',
			'^([Ff]ilterlist)$',
			'^(فیلتر لیست)$',
			'^[/!#]([Ff]ilterlist [Cc]lean)$',
			'^([Ff]ilterlist [Cc]lean)$',
			'^(حذف فیلتر لیست)$',
			'^([Uu]n[Mm]uteall)$',
			'^([Mm]uteall)$',
			'^(سکوت همه)$',
			'^(سکوت همه) (.*)$',
			'^([Mm]uteall) (.*)$',
			'^[/!#]([Uu]n[Mm]uteall)$',
			'^[/!#]([Mm]uteall)$',
			'^[/!#]([Mm]uteall) (.*)$',
			'^([Bb]lock) (.*)$',
			'^([Uu]n[Bb]lock) (.*)$',
			'^[/!#]([Bb]lock) (.*)$',
			'^[!#/]([Uu]n[Bb]lock) (.*)$',
			'^([Bb]anall)$',
			'^([Uu]n[Bb]anall)$',
			'^[/!#]([Bb]anall)$',
			'^([Bb]analllist)$',
			'^[/!#]([Bb]analllist)$',
			'^[!#/]([Uu]n[Bb]anall)$',

			},
	--		Pre = Pre,
			run = Run
		},
		CheckMethod = 'F80', -- Also Can use as 'TeleSeed' ! # If use TeleSeed Input Will be msg = { to = {}, from = {}} :))
	--	Cron = Cron
	}