	ChangeStats2 = function(X, Y, Z)
		X = X:gsub(X:sub(1,1), X:sub(1,1):upper())
		redis:hset(Y, X, Z)
	end

	StatsGET = function (X, Chat_ID)
		if X == 'OK' then
			return Language(Chat_ID, '🗽Allow')
		elseif X == 'DEL' then
			return Language(Chat_ID, '🗑Clean')
		elseif X == 'WARN' then
			return Language(Chat_ID, '⚠️Warn')
		else
			return X
		end
	end

	getV = function (X)
		X = X:gsub("flood", "فلود")
		X = X:gsub("inline", "درون خطی")
		X = X:gsub("keyboard", "کیبورد")
		X = X:gsub("document", "فایل")
		X = X:gsub("media", "رسانه")
		X = X:gsub("webpage", "وب پیج")
		X = X:gsub("tgservice", "تلگرام سرویس")
		X = X:gsub("link", "لینک")
		X = X:gsub("mention", "منشن")
		X = X:gsub("atsign", "ات ساین")
		X = X:gsub("hashtag", "هشتگ")
		X = X:gsub("persion", "پارسی")
		X = X:gsub("english", "انگلیسی")
		X = X:gsub("edit", "ویرایش")
		X = X:gsub("photo", "عکس")
		X = X:gsub("video", "فیلم")
		X = X:gsub("contact", "شماره تلفن")
		X = X:gsub("music", "اهنگ")
		X = X:gsub("voice", "ویس")
		X = X:gsub("location", "مکان")
		X = X:gsub("animation", "انیمیشن")
		X = X:gsub("sticker", "استیکر")
		X = X:gsub("game", "گیم")
		X = X:gsub("forward", "فوروارد")
		return X
	end


	function Run(msg, matches)
		if #matches > 0 then
		if isMod(msg.sender_user_id_, msg.chat_id_)  then
			USERNAME = getUserInfo(msg.sender_user_id_)
			if (matches[1]:lower() == "settings" or matches[1] == "تنظیمات") then 
				XTEXT =" \n"
				if (redis:hget(msg.chat_id_, 'Lang') or 'en') == 'en' then
					for k , v in pairs({
						'link', 'mention', 'atsign', 'hashtag', 
						'persion', 'english', 'edit', 'photo', 
						'video', 'contact', 'music', 'voice', 
						'location', 'animation', 'sticker', 
						'game', 'inline', 'keyboard', 
						'document', 'media', 'webpage', 
						'tgservice','flood','forward'
					}) do 
						X = v:gsub(v:sub(1,1), v:sub(1,1):upper())
						GP = redis:hget(msg.chat_id_, X)
						if not GP then 
							GP = "OK"
							redis:hset(msg.chat_id_, v, "OK") 
						end		
						XTEXT = XTEXT .."> *Lock ".. v .. "* : `" .. StatsGET(GP, msg.chat_id_) .. "`\n"
					end
					XTEXT = XTEXT .."> *Lock ".. "Flood Count" .. "* : `" .. redis:hget(msg.chat_id_, "FloodC") .. "`\n"
					XTEXT = XTEXT .."> *Lock ".. "Flood Time" .. "* : `" .. redis:hget(msg.chat_id_, "FloodT") .. "`\n"
					text = XTEXT.."> *Expire* : `" .. math.floor( ( tonumber((redis:hget(msg.chat_id_, "ExpireTime") or os.time()) - tonumber(os.time()) )) / 60 / 60 / 24) + 1 .. "` *Days*"
				else 
					for k , v in pairs({
						'link', 'mention', 'atsign', 'hashtag', 
						'persion', 'english', 'edit', 'photo', 
						'video', 'contact', 'music', 'voice', 
						'location', 'animation', 'sticker', 
						'game', 'inline', 'keyboard', 
						'document', 'media', 'webpage', 
						'tgservice','flood','forward'
					}) do 
						X = v:gsub(v:sub(1,1), v:sub(1,1):upper())
						GP = redis:hget(msg.chat_id_, X)
						if not GP then 
							GP = "OK"
							redis:hset(msg.chat_id_, v, "OK") 
						end		
						XTEXT = XTEXT .."> *قفل ".. getV(v) .. "* : `" .. StatsGET(GP, msg.chat_id_) .. "`\n"
					end
					XTEXT = XTEXT .."> *قفل ".. "تعداد رگباری" .. "* : `" .. redis:hget(msg.chat_id_, "FloodC") .. "`\n"
					XTEXT = XTEXT .."> *قفل ".. "زمان رگباری" .. "* : `" .. redis:hget(msg.chat_id_, "FloodT") .. "`\n"
					text = XTEXT.."> *انقضا* : `" .. math.floor( ( tonumber((redis:hget(msg.chat_id_, "ExpireTime") or os.time()) - tonumber(os.time()) )) / 60 / 60 / 24) + 1 .. "` *روز*"
				end
				cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, text, 1, 'MarkDown')
			end
			if (matches[1]:lower() == "lock" or matches[1] == "قفل")  then
				chat_id = msg.chat_id_
				if ( matches[2] ==  'link' or matches[2] == "لینک") then
					X = 'link'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 لینک تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت لینک فعال شد🚫
⚠️گروه در حال قفل لینک میباشد و تمامی لینک ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'mention'  or matches[2] == "منشن") then
					X = 'mention'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 یوزرنیم تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت یوزرنیم فعال شد🚫
⚠️گروه در حال قفل یوزرنیم میباشد و تمامی یوزرنیم ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'forward'  or matches[2] == "فوروارد") then
					X = 'forward'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 فوروارد تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت فوروارد فعال شد🚫
⚠️گروه در حال قفل فوروارد میباشد و تمامی فوروارد ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'atsign'  or matches[2] == "یوزرنیم") then
					X = 'atsign'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 یوزرنیم تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت یوزرنیم فعال شد🚫
⚠️گروه در حال قفل یوزرنیم میباشد و تمامی یوزرنیم ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'hashtag'  or matches[2] == "هشتگ") then
					X = 'hashtag'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 هشتگ تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت هشتگ فعال شد🚫
⚠️گروه در حال قفل هشتگ میباشد و تمامی هشتگ ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'persian'  or matches[2] == "فارسی") then
					X = 'persion'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 فارسی تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت فارسی فعال شد🚫
⚠️گروه در حال قفل فارسی میباشد و تمامی فارسی ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'english'  or matches[2] == "انگلیسی") then
					X = 'english'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 انگلیسی تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت انگلیسی فعال شد🚫
⚠️گروه در حال قفل انگلیسی میباشد و تمامی انگلیسی ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'edit'  or matches[2] == "ویرایش") then
					X = 'edit'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 ویرایش تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت ویرایش فعال شد🚫
⚠️گروه در حال قفل ویرایش میباشد و تمامی ویرایش ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'photo'  or matches[2] == "عکس") then
					X = 'photo'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 انگلیسی تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت عکس فعال شد🚫
⚠️گروه در حال قفل عکس میباشد و تمامی عکس ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'video'  or matches[2] == "فیلم") then
					X = 'video'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 فیلم تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت فیلم فعال شد🚫
⚠️گروه در حال قفل فیلم میباشد و تمامی فیلم ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'contact'  or matches[2] == "شماره") then
					X = 'contact'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 شماره تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت شماره فعال شد🚫
⚠️گروه در حال قفل شماره میباشد و تمامی شماره ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'music'  or matches[2] == "موسیقی") then
					X = 'music'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 انگلیسی تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت موسیقی فعال شد🚫
⚠️گروه در حال قفل موسیقی میباشد و تمامی موسیقی  ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'voice'  or matches[2] == "ویس") then
					X = 'voice'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 ویس تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت ویس فعال شد🚫
⚠️گروه در حال قفل ویس میباشد و تمامی ویس  ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'location'  or matches[2] == "مکان") then
					X = 'location'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 مکان تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت مکان فعال شد🚫
⚠️گروه در حال قفل مکان میباشد و تمامی مکان ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'animation'  or matches[2] == "گیف") then
					X = 'animation'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 گیف تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت گیف فعال شد🚫
⚠️گروه در حال قفل گیف میباشد و تمامی گیف ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'sticker'  or matches[2] == "استیکر") then
					X = 'sticker'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 استیکر تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت استیکر فعال شد🚫
⚠️گروه در حال قفل استیکر میباشد و تمامی استیکر ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'game'  or matches[2] == "بازی") then
					X = 'game'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 بازی تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت بازی فعال شد🚫
⚠️گروه در حال قفل بازی میباشد و تمامی بازی ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'inline'  or matches[2] == "درون خطی") then
					X = 'inline'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 درون خطی تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت درون خطی فعال شد🚫
⚠️گروه در حال قفل درون خطی میباشد و تمامی درون خطی ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'keyboard'  or matches[2] == "کیبورد") then
					X = 'keyboard'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 کیبورد تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت کیبورد فعال شد🚫
⚠️گروه در حال قفل کیبورد میباشد و تمامی کیبورد ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'document'  or matches[2] == "فایل") then
					X = 'document'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 فایل تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت فایل فعال شد🚫
⚠️گروه در حال قفل فایل میباشد و تمامی فایل ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'media'  or matches[2] == "رسانه") then
					X = 'media'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 رسانه تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت رسانه فعال شد🚫
⚠️گروه در حال قفل رسانه میباشد و تمامی رسانه ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'webpage'  or matches[2] == "صفحه وب") then
					X = 'webpage'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 صفحه وب تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت صفحه وب فعال شد🚫
⚠️گروه در حال قفل صفحه وب میباشد و تمامی صفحه وب ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'tgservice'  or matches[2] == "پیام سرویس") then
					X = 'tgservice'
					ChangeStats2(X, chat_id, "DEL")
					return [[🔏 پیام سرویس تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت پیام سرویس فعال شد🚫
⚠️گروه در حال قفل پیام سرویس میباشد و تمامی پیام سرویس ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				elseif ( matches[2] == 'flood'  or matches[2] == "رگباری") then
					X = 'flood'
					ChangeStats2(X, chat_id, "OK")
					return [[🔏 رگباری تا اطلاع ثانوے بسته شد.
━━━━━━━━━━━━
🚫ممنوعیت رگباری فعال شد🚫
⚠️گروه در حال قفل رگباری میباشد و تمامی رگباری ها توسط ربات هوشمند #پاک خواهد شد🗑
━━━━━━━━━━━━
❗️لطفا لینک نفرستید

قفل شد توسط :]]..USERNAME
				else
		
				end
			end

			
			if (matches[1]:lower() == "unlock" or matches[1] == "باز کردن")  then
				chat_id = msg.chat_id_
				if ( matches[2] == 'link'  or matches[2] == "لینک") then
					X = 'link'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 لینک از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن لینک خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'forward'  or matches[2] == "فوروارد") then
					X = 'forward'
					ChangeStats2(X, chat_id, "DEL")
					return [[
🔓 فوروارد از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن فوروارد خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'mention'  or matches[2] == "منشن") then
					X = 'mention'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 منشن از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن منشن خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'flood'  or matches[2] == "رگباری") then
					X = 'flood'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 رگباری از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن رگباری خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'atsign'  or matches[2] == "یوزرنیم") then
					X = 'atsign'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 یوزرنیم از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن یوزرنیم خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'hashtag'  or matches[2] == "هشتگ") then
					X = 'hashtag'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 هشتگ از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن هشتگ خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'persian'  or matches[2] == "فارسی") then
					X = 'persion'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 فارسی از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن فارسی خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'english'  or matches[2] == "انگلیسی") then
					X = 'english'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 انگلیسی از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن انگلیسی خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'edit'  or matches[2] == "ویرایش") then
					X = 'edit'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 ویرایش از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن ویرایش خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'photo'  or matches[2] == "عکس") then
					X = 'photo'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 عکس از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن عکس خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'video'  or matches[2] == "فیلم") then
					X = 'video'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 فیلم از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن فیلم خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'contact'  or matches[2] == "شماره") then
					X = 'contact'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 شماره از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن شماره خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'music'  or matches[2] == "موسیقی") then
					X = 'music'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 موسیقی از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن موسیقی خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'voice'  or matches[2] == "ویس") then
					X = 'voice'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 ویس از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن ویس خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'location'  or matches[2] == "مکان") then
					X = 'location'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 مکان از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن مکان خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'animation'  or matches[2] == "گیف") then
					X = 'animation'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 گیف از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن گیف خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'sticker'  or matches[2] == "استیکر") then
					X = 'sticker'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 استیکر از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن استیکر خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'game'  or matches[2] == "بازی") then
					X = 'game'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 بازی از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن بازی خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'inline'  or matches[2] == "درون خطی") then
					X = 'inline'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 درون خطی از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن درون خطی خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'keyboard'  or matches[2] == "کیبورد") then
					X = 'keyboard'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 کیبورد از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن کیبورد خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'document'  or matches[2] == "فایل") then
					X = 'document'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 فایل از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن فایل خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'media'  or matches[2] == "رسانه") then
					X = 'media'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 رسانه از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن رسانه خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'webpage'  or matches[2] == "صفحه وب") then
					X = 'webpage'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 صفحه وب از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن صفحه وب خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				elseif ( matches[2] == 'tgservice'  or matches[2] == "پیام سرویس") then
					X = 'tgservice'
					ChangeStats2(X, chat_id, "OK")
					return [[
🔓 پیام سرویس از وضعیت بسته خارج شد.
😎 اڪنون ڪاربران مےتوانند به فرستادن پیام سرویس خود ادامه دهند.
انجام شد توسط :]]..USERNAME
				else
				end
			end
		end
		end
	end
	return  {
		cli = {
			_MSG = {
				'^([Ss]ettings)$',
				'^(تنظیمات)$',
				'^[/!#]([Ss]ettings)$',
				'^([Ll]ock) (.*)$',
				'^(قفل) (.*)$',
				'^[/!#]([Ll]ock) (.*)$',
				'^[/!#]([Uu]n[Ll]ock) (.*)$',
				'^([Uu]n[Ll]ock) (.*)$',
				'^(باز کردن) (.*)$',
			},
	--		Pre = Pre,
			run = Run
		},
		CheckMethod = 'f80', 
	}