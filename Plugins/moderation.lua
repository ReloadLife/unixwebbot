	--[[
		#         SPR-CPU 			#
		#       Modertation Plugin 	#
		#  	Usable by Bot Mods+     #
		#	 Update : 9/May/2017 	#
	]]
	local utf8 = require 'lua-utf8' -- Need For Count Chars for Mention !
	local cli = cli
	local api = api
	local redis = redis
	function Run(msg, matches)
		if #matches > 0 then
				if ( matches[1]:lower() =='promote' or matches[1]:lower() == "ارتقاع") and isOwner(msg.sender_user_id_, msg.chat_id_) then --Just for owners!
					if msg.reply_to_message_id_ ~= 0 then --if is a Replyed Message!
						cli.getMessage(
							msg.chat_id_,
							msg.reply_to_message_id_,
							function (extra, result)
								local user_id = result.sender_user_id_
								local chat_id = msg.chat_id_
								if isMod(user_id, chat_id) then
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "ERROR_PROMOTE")
									local TEXTtoSend = text .. user .. Language(chat_id, "ERROR_PROMOTE2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								else
									redis:sadd('Mods'..chat_id, user_id)
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "SUCCESS_PROMOTE")
									local TEXTtoSend = text .. user .. Language(chat_id, "SUCCESS_PROMOTE2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								end
							end,
							nil
						)
					end
					if #matches > 1 then
						if #msg.content_.entities_ > 0 then
							if msg.content_.entities_[0].ID == "MessageEntityMentionName" then -- if matches[2] is A Mention Name!
								X = tonumber(msg.content_.entities_[0].user_id_)
							else
								X = false
							end
						else
							if tonumber(matches[2]) ~= nil then
								X = tonumber(matches[2])
							else
								X = false
							end
						end
						local X = X
						if X then
							cli.getUserFull(
								X,
								function (extra, result)
									if result.ID == 'Error' then -- if there is a Error in GetUserFull !!
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										local user_id = result.user_.id_
										local chat_id = msg.chat_id_
										if isMod(result.user_.id_, msg.chat_id_) then
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "ERROR_PROMOTE")
											local TEXTtoSend = text .. user .. Language(chat_id, "ERROR_PROMOTE2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										else
											redis:sadd('Mods'..chat_id, user_id)
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "SUCCESS_PROMOTE")
											local TEXTtoSend = text .. user .. Language(chat_id, "SUCCESS_PROMOTE2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										end
									end
								end,
								nil
							)
						else
							cli.searchPublicChat(
								matches[2]:gsub('@', ''),
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										if result.type_.ID == 'PrivateChatInfo' then 
											user_id = result.id_ 
											chat_id = msg.chat_id_ 
											if isMod(result.id_, msg.chat_id_) then
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "ERROR_PROMOTE")
												local TEXTtoSend = text .. user .. Language(chat_id, "ERROR_PROMOTE2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
											else
												redis:sadd('Mods'..chat_id, user_id)
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "SUCCESS_PROMOTE")
												local TEXTtoSend = text .. user .. Language(chat_id, "SUCCESS_PROMOTE2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
											end
										else
											return Language(chat_id, "CHANNEL_CANT_PROMOTE")
										end
									end
								end,
							nil)
						end
					end
				end
				if ( matches[1]:lower() =='demote' or matches[1]:lower() == "تنزل") and isOwner(msg.sender_user_id_, msg.chat_id_) then
					if msg.reply_to_message_id_ ~= 0 then
						cli.getMessage(
							msg.chat_id_,
							msg.reply_to_message_id_,
							function (extra, result)
								local user_id = result.sender_user_id_
								local chat_id = msg.chat_id_
								if not isMod(user_id, chat_id) then
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "ERROR_DEMOTE")
									local TEXTtoSend = text .. user .. Language(chat_id, "ERROR_DEMOTE2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
								else
									redis:srem('Mods'..chat_id, user_id)
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "SUCCESS_DEMOTE")
									local TEXTtoSend = text .. user .. Language(chat_id, "SUCCESS_DEMOTE2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								end
							end,
							nil
						)
					end
					if #matches > 1 then
						if #msg.content_.entities_ > 0 then
							if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
								X = tonumber(msg.content_.entities_[0].user_id_)
							else
								X = nil
							end
						else
							if tonumber(matches[2]) ~= nil then
								X = tonumber(matches[2])
							else
								X = nil
							end
						end
						local X = X
						if type(X) == 'number' then
							cli.getUserFull(
								X,
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										local user_id = result.user_.id_
										local chat_id = msg.chat_id_
										if not isMod(user_id, chat_id) then
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "ERROR_DEMOTE")
											local TEXTtoSend = text .. user .. Language(chat_id, "ERROR_DEMOTE2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
										else
											redis:srem('Mods'..chat_id, user_id)
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "SUCCESS_DEMOTE")
											local TEXTtoSend = text .. user .. Language(chat_id, "SUCCESS_DEMOTE2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										end
									end
								end,
								nil
							)
						else
							cli.searchPublicChat(
								matches[2]:gsub('@', ''),
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										if result.type_.ID == 'PrivateChatInfo' then 
											local user_id = result.id_ 
											local chat_id = msg.chat_id_ 
											if not isMod(user_id, chat_id) then
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "ERROR_DEMOTE")
												local TEXTtoSend = text .. user .. Language(chat_id, "ERROR_DEMOTE2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
											else
												redis:srem('Mods'..chat_id, user_id)
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "SUCCESS_DEMOTE")
												local TEXTtoSend = text .. user .. Language(chat_id, "SUCCESS_DEMOTE2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
											end
										else
											return Language(chat_id, "CHANNEL_CANT_PROMOTE")
										end
									end
								end,
							nil)
						end
					end
				end
				if ( matches[1]:lower() =='mods' or matches[1]:lower() == 'مدیران') then
					if matches[2] :lower() == ('clean' or 'حذف') and isOwner(msg.sender_user_id_, msg.chat_id_)  then
						redis:del('Mods'..msg.chat_id_)
						return Language(msg.chat_id_, "CLEAN_MODS_SUCCESS")
					elseif matches[2] :lower() == ('list' or 'لیست') then
						if redis:scard('Mods'..msg.chat_id_) == 0 then -- If no moderators!
							return Language(msg.chat_id_, "NO_MODS")
						else
							local Mods = Language(msg.chat_id_, "LIST_MODS") .. (redis:hget(msg.chat_id_, 'Title') or msg.chat_id_).. '\n'
							for k, v in pairs(redis:smembers('Mods'..msg.chat_id_)) do
								Mods = Mods.. k ..') '..getUserInfo(v) .. '\n'
							end
							return Mods
						end
					end
				end
				if ( matches[1]:lower() =='muteuser' or matches[1]:lower() == 'ساکت') and isMod(msg.sender_user_id_, msg.chat_id_) then
					if msg.reply_to_message_id_ ~= 0 then
						cli.getMessage(
							msg.chat_id_,
							msg.reply_to_message_id_,
							function (extra, result)
								local user_id = result.sender_user_id_
								local chat_id = msg.chat_id_
								if redis:sismember('Mutelist'..chat_id, user_id) then
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "MUTE_ERROR")
									local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_ERROR2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
								else
									if isMod(user_id, chat_id) then
										local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										local text = Language(chat_id, "MUTE_ERROR1")
										local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_ERROR12")
										cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
									else
										redis:sadd('Mutelist'..chat_id, user_id)
										local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										local text = Language(chat_id, "MUTE_SUCCESS")
										local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_SUCCESS2")
										cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
									end
								end
							end,
						nil)
					end
					if #matches > 1 then
						if #msg.content_.entities_ > 0 then
							if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
								X = tonumber(msg.content_.entities_[0].user_id_)
							else
								X = false
							end
						else
							if tonumber(matches[2]) ~= nil then
								X = tonumber(matches[2])
							else
								X = false
							end
						end
						if X then
							cli.getUserFull(
								X,
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										user_id = result.user_.id_
										chat_id = msg.chat_id_
										if redis:sismember('Mutelist'..chat_id, user_id) then
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "MUTE_ERROR")
											local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_ERROR2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
										else
											if isMod(user_id, chat_id) then
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "MUTE_ERROR1")
												local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_ERROR12")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
											else
												redis:sadd('Mutelist'..chat_id, user_id)
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "MUTE_SUCCESS")
												local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_SUCCESS2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
											end
										end
									end
								end,
								nil
							)
						else
							cli.searchPublicChat(
								matches[2]:gsub('@', ''),
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										if result.type_.ID == 'PrivateChatInfo' then 
											local user_id = result.id_ 
											local chat_id = msg.chat_id_ 
											if redis:sismember('Mutelist'..chat_id, user_id) then
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "MUTE_ERROR")
												local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_ERROR2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
											else
												if isMod(user_id, chat_id) then
													local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
													local text = Language(chat_id, "MUTE_ERROR1")
													local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_ERROR12")
													cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
												else
													redis:sadd('Mutelist'..chat_id, user_id)
													local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
													local text = Language(chat_id, "MUTE_SUCCESS")
													local TEXTtoSend = text .. user .. Language(chat_id, "MUTE_SUCCESS2")
													cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user)) 
												end
											end

										else
											return Language(chat_id, "CHANNEL_CANT_PROMOTE")
										end
									end
								end,
								nil
							)
						end
					end
				end
				if ( matches[1]:lower() =='unmuteuser' or matches[1]:lower() == 'رفع ساکت') and isMod(msg.sender_user_id_, msg.chat_id_) then
					if msg.reply_to_message_id_ ~= 0 then
						cli.getMessage(
							msg.chat_id_,
							msg.reply_to_message_id_,
							function (extra, result)
								local user_id = result.sender_user_id_
								local chat_id = msg.chat_id_
								if not redis:sismember('Mutelist'..chat_id, user_id) then
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "UNMUTE_ERROR")
									local TEXTtoSend = text .. user .. Language(chat_id, "UNMUTE_ERROR2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								else
									redis:srem('Mutelist'..chat_id, user_id)
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "UNMUTE_SUCCESS")
									local TEXTtoSend = text .. user .. Language(chat_id, "UNMUTE_SUCCESS2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								end
							end,
							nil
						)
					end
					if #matches > 1 then
						if #msg.content_.entities_ > 0 then
							if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
								X = tonumber(msg.content_.entities_[0].user_id_)
							else
								X = false
							end
						else
							if tonumber(matches[2]) ~= nil then
								X = tonumber(matches[2])
							else
								X = false
							end
						end
						if X then
							cli.getUserFull(
								X,
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										local user_id = result.user_.id_
										local chat_id = msg.chat_id_
										if not redis:sismember('Mutelist'..chat_id, user_id) then
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "UNMUTE_ERROR")
											local TEXTtoSend = text .. user .. Language(chat_id, "UNMUTE_ERROR2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										else
											redis:srem('Mutelist'..chat_id, user_id)
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "UNMUTE_SUCCESS")
											local TEXTtoSend = text .. user .. Language(chat_id, "UNMUTE_SUCCESS2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										end
									end
								end,
								nil
							)
						else
							cli.searchPublicChat(
								matches[2]:gsub('@', ''),
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										if result.type_.ID == 'PrivateChatInfo' then 
											user_id = result.id_ 
											chat_id = msg.chat_id_ 
											if not redis:sismember('Mutelist'..chat_id, user_id) then
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "UNMUTE_ERROR")
												local TEXTtoSend = text .. user .. Language(chat_id, "UNMUTE_ERROR2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
											else
												redis:srem('Mutelist'..chat_id, user_id)
												local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
												local text = Language(chat_id, "UNMUTE_SUCCESS")
												local TEXTtoSend = text .. user .. Language(chat_id, "UNMUTE_SUCCESS2")
												cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
											end
										else
											return Language(chat_id, "CHANNEL_CANT_PROMOTE")
										end
									end
								end,
								nil
							)
						end
					end
				end
				if ( matches[1]:lower() =='mutes' or matches[1]:lower() == 'سکوت') then
					if matches[2] :lower() == ('clean' or 'حذف') and isOwner(msg.sender_user_id_, msg.chat_id_)  then
						redis:del('Mutelist'..msg.chat_id_)
						return Language(msg.chat_id_, "MUTES_CLEAN")
					elseif matches[2] :lower() == ('list' or 'لیست') then
						if redis:scard('Mutelist'..msg.chat_id_) == 0 then
							return Language(msg.chat_id_, "NULL_MUTELIST")
						else
							local Mutelist = Language(msg.chat_id_, "MUTE_LIST") .. (redis:hget(msg.chat_id_, 'Title') or msg.chat_id_).. '\n'
							for k, v in pairs(redis:smembers('Mutelist'..msg.chat_id_)) do
								Mutelist = Mutelist.. k ..') '..getUserInfo(v) .. '\n'
							end
							return Mutelist
						end
					end
				end
				if ( matches[1]:lower() =='kick' or matches[1]:lower() == 'اخراج') and isMod(msg.sender_user_id_, msg.chat_id_) then
					if msg.reply_to_message_id_ ~= 0 then
						cli.getMessage(
							msg.chat_id_,
							msg.reply_to_message_id_,
							function (extra, result)
								local user_id = result.sender_user_id_
								local chat_id = msg.chat_id_
								if isMod(user_id, chat_id) then
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "KICK_ERROR")
									local TEXTtoSend = text .. user .. Language(chat_id, "KICK_ERROR2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								else
									cli.changeChatMemberStatus(chat_id, user_id, 'Kicked')
									local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									local text = Language(chat_id, "KICK_SUCCESS")
									local TEXTtoSend = text .. user .. Language(chat_id, "KICK_SUCCESS2")
									cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
								end
							end,
							nil
						)
					end
					if #matches > 1 then
						if #msg.content_.entities_ > 0 then
							if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
								X = tonumber(msg.content_.entities_[0].user_id_)
							else
								X = false
							end
						else
							if tonumber(matches[2]) ~= nil then
								X = tonumber(matches[2])
							else
								X = false
							end
						end
						if X then
							cli.getUserFull(
								X,
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										user_id = result.user_.id_
										chat_id = msg.chat_id_
									if isMod(user_id, chat_id) then
										local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										local text = Language(chat_id, "KICK_ERROR")
										local TEXTtoSend = text .. user .. Language(chat_id, "KICK_ERROR2")
										cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
									else
										cli.changeChatMemberStatus(chat_id, user_id, 'Kicked')
										local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										local text = Language(chat_id, "KICK_SUCCESS")
										local TEXTtoSend = text .. user .. Language(chat_id, "KICK_SUCCESS2")
										cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
									end
								end
								end,
								nil
							)
						else
							cli.searchPublicChat(
								matches[2]:gsub('@', ''),
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										if result.type_.ID == 'PrivateChatInfo' then 
											user_id = result.id_ 
											chat_id = msg.chat_id_ 
											user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										if isMod(user_id, chat_id) then
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "KICK_ERROR")
											local TEXTtoSend = text .. user .. Language(chat_id, "KICK_ERROR2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										else
											cli.changeChatMemberStatus(chat_id, user_id, 'Kicked')
											local user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
											local text = Language(chat_id, "KICK_SUCCESS")
											local TEXTtoSend = text .. user .. Language(chat_id, "KICK_SUCCESS2")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, TEXTtoSend, utf8.len(text), utf8.len(user))
										end
										else
											return Language(chat_id, "CHANNEL_CANT_PROMOTE")
										end
									end
								end,
							nil)
						end
					end
				end
				if ( matches[1]:lower() =='invite' or matches[1]:lower() == 'دعوت') and isMod(msg.sender_user_id_, msg.chat_id_) then
					if msg.reply_to_message_id_ ~= 0 then
						cli.getMessage(
							msg.chat_id_,
							msg.reply_to_message_id_,
							function (extra, result)
								local user_id = result.sender_user_id_
								local chat_id = msg.chat_id_
									user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
									cli.addChatMember(chat_id, user_id, 50, function (Arg, Data)
										if Data.ID == 'Ok' then
											text = user .. Language(chat_id, "INV_SUCS")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, text, 0, utf8.len(user))
										elseif Data.ID == 'Error' then
											Text = Language(chat_id, "INV_ERROR")..MarkScape(Data.message_)
											cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, Text, 1, 'MarkDown')
										end
									end, nil) 
							end,
							nil
						)
					end
					if #matches > 1 then
						if #msg.content_.entities_ > 0 then
							if msg.content_.entities_[0].ID == "MessageEntityMentionName" then
								X = tonumber(msg.content_.entities_[0].user_id_)
							else
								X = false
							end
						else
							if tonumber(matches[2]) ~= nil then
								X = tonumber(matches[2])
							else
								X = false
							end
						end
						if X then
							cli.getUserFull(
								X,
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										user_id = result.user_.id_
										chat_id = msg.chat_id_
										user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										cli.addChatMember(chat_id, user_id, 50, function (Arg, Data)
										if Data.ID == 'Ok' then
											text = user .. Language(chat_id, "INV_SUCS")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, text, 0, utf8.len(user))
										elseif Data.ID == 'Error' then
											Text = Language(chat_id, "INV_ERROR")..MarkScape(Data.message_)
											cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, Text, 1, 'MarkDown')
										end
									end, nil) 
									end
								end,
								nil
							)
						else
							cli.searchPublicChat(
								matches[2]:gsub('@', ''),
								function (extra, result)
									if result.ID == 'Error' then
										return Language(msg.chat_id_, "Error !\n Error Result : ")
									else
										if result.type_.ID == 'PrivateChatInfo' then 
											user_id = result.id_ 
											chat_id = msg.chat_id_ 
										user = tostring(getUserInfo(user_id)):gsub('\\', ''):gsub('@','')
										cli.addChatMember(chat_id, user_id, 50, function (Arg, Data)
										if Data.ID == 'Ok' then
											text = user .. Language(chat_id, "INV_SUCS")
											cli.sendMention(chat_id, user_id, msg.reply_to_message_id_, text, 0, utf8.len(user))
										elseif Data.ID == 'Error' then
											Text = Language(chat_id, "INV_ERROR")..MarkScape(Data.message_)
											cli.sendText(msg.chat_id_, msg.id_, 0, 1, nil, Text, 1, 'MarkDown')
										end
									end, nil) 
										else
											return Language(chat_id, "CHANNEL_CANT_PROMOTE")
										end
									end
								end,
								nil
							)
						end
					end
				end
		end
	end
	return {
		cli = {
			_MSG = {
				'^([Pp]romote) (.*)$',
				'^(ارتقاع) (.*)$',
				'^([Pp]romote)$',
				'^(ارتقاع)$',
				'^([Dd]emote)$',
				'^(تنزل)$',
				'^(تنزل) (.*)$',
				'^([Dd]emote) (.*)$',
				'^([Mm]ods) ([Cc]lean)$',
				'^(مدیران) (حذف)$',
				'^(مدیران) (لیست)$',
				'^[/!#]([Pp]romote) (.*)$',
				'^[/!#]([Pp]romote)$',
				'^[/!#]([Dd]emote)$',
				'^[/!#]([Dd]emote) (.*)$',
				'^[/!#]([Mm]ods) ([Cc]lean)$',
				'^[/!#]([Mm]ods) ([Ll]ist)$',
				'^([Mm]uteuser) (.*)$',
				'^(ساکت) (.*)$',
				'^([Mm]uteuser)$',
				'^(ساکت)$',
				'^([Uu]nmuteuser)$',
				'^(رفع ساکت)$',
				'^([Uu]nmuteuser) (.*)$',
				'^(رفع ساکت) (.*)$',
				'^([Mm]utes) ([Cc]lean)$',
				'^(سکوت) (حذف)$',
				'^([Mm]utes) ([Ll]ist)$',
				'^(سکوت) (لیست)$',
				'^[/!#]([Mm]uteuser) (.*)$',
				'^[/!#]([Mm]uteuser)$',
				'^[/!#]([Uu]nmuteuser)$',
				'^[/!#]([Uu]nmuteuser) (.*)$',
				'^[/!#]([Mm]utes) ([Cc]lean)$',
				'^[/!#]([Mm]utes) ([Ll]ist)$',
				'^([Ii]nvite) (.*)$',
				'^(دعوت) (.*)$',
				'^([Ii]nvite)$',
				'^(دعوت)$',
				'^([Kk]ick)$',
				'^(اخراج)$',
				'^([Kk]ick) (.*)$',
				'^(اخراج) (.*)$',
			},
	--		Pre = Pre,
			run = Run
		},
		CheckMethod = 'f80', -- Also Can use as 'TeleSeed' ! # If use TeleSeed Input Will be msg = { to = {}, from = {}} :))
	--	Cron = Cron
	}