    --[[
        Bot . Lua By @ReloadLife
        WebSite : https://ReloadLife.me
        Version : V1.0.0
    ]]
	https = require "ssl.https"
	HTTPS = require "ssl.https"
	json = require "cjson"
	serpent = require "serpent"
	URL = require 'socket.url'
	url = require 'socket.url'
	JSON = require 'cjson'
	clr = require 'term.colors'
	cli = dofile('./Lib.lua').cli
    Redis = require 'redis'
    redis = Redis.connect('127.0.0.1', 6379)
	UTF8 = require 'lua-utf8'
	function LoadPlugins ()
		_Config = dofile('./Config.lua')
	end
    require('Ranks')
    require('Langs')
	function getUserInfo(user_ID)
		
		if redis:hget(user_ID, 'username') then
    		USER = redis:hgetall(user_ID)
			if USER.username then
   				return (USER.username)
			elseif USER.firstname then
   				return (USER.firstname)
    		else
    			return user_ID
			end
		else
			redis:del(user_ID)
    		return user_ID
		end
		return user_ID
	end
	function VarDump(Value)
		print(clr.blue..serpent.block(Value,{comment=false})..clr.reset)
	end
	LoadPlugins ()
	function DoMessage(msg)
		if msg then
			chat_id = msg.chat_id_ 
			user_id = msg.sender_user_id_
			Group = redis:hgetall(chat_id)
			msg_id = msg.id_
			GP = Group
			C = tonumber(redis:get(user_id.."usermsgs") or 0) + 1
			redis:set(user_id.."usermsgs",C)
			TEXT = (msg.content_.text_ or msg.content_.caption_ or ' ')
			if tostring(chat_id):match('-') then
				redis:sadd('Groups!', msg.chat_id_)
			end
			cli.getChannelFull(chat_id, 
					function (arg, data)
						redis:hset(arg, 'AdminCount', data.administrator_count_)
						redis:hset(arg, 'MembersCount', data.member_count_)
						redis:hset(arg, 'Blocked', data.kicked_count_)
						redis:hset(arg, 'About', data.about_)
						if data.invite_link_ then
							redis:hset(arg, 'GroupLink', data.invite_link_)
						end
					end,
					chat_id)
				cli.getChannelMembers(chat_id, 'Administrators', 0, 100, 
					function (Arg, data)
						if #data.members_ > 0 then
							for k, v in pairs(data.members_) do
								if v.status_.ID == 'ChatMemberStatusCreator' then
									redis:sadd(v.user_id_..'Chats', Arg)
									redis:hset(Arg, 'Owner', v.user_id_)
									redis:sadd('Mods'..Arg, v.user_id_)
								end
								if v.status_.ID == 'ChatMemberStatusEditor' then
									redis:sadd('Mods'..Arg, v.user_id_)
								end
							end
						end
					end, chat_id)
				cli.getChat(chat_id,
					function (arg, data)
						if data.title_ then
							redis:hset(arg, 'Title', data.title_)
						end
					end,
					chat_id
				)
				if msg.content_.ID == "MessageChatJoinByLink"  then
					if (msg.sender_user_id_) == _Config.BotID then
						cli.getMe(function (a, d)
							id = d.id_
							name = d.first_name_
							phone = d.phone_number_
							cli.sendContact(chat_id, 0, 0, 1, nil, phone, name, '', id, function (a, d2)
								TEXT = [[
سلام!
مدیر این گروه درخاست ربات ضد لینک کرده بود!
اکنون من به این گروه پیوستم!
ابتدا شماره به اشتراک گذاشته شده در بالا را سیو کنید!
سپس من را ادمین گروه کنید تا فعالیتم رو شروع کنم!
برای دردن دستورات ربات از دستور `help` و برای انجام تنظیمات از دستور `settings` استفاده کنید!
اگه ربات مشکلی داشت به @Reload\_LifeBot مراجعه کنید !
]]
								cli.sendText(chat_id, d2.id_, 0, 1, nil, TEXT, 1, 'md', function (a, d3)
								end, nil)
							end, nil)
						end, nil)
					end
				end
				if msg.content_.ID == "MessageChatAddMembers" then
					if (msg.content_.members_[0].id_) == _Config.BotID then
						cli.getMe(function (a, d)
							id = d.id_
							name = d.first_name_
							phone = d.phone_number_
							cli.sendContact(chat_id, 0, 0, 1, nil, phone, name, '', id, function (a, d2)
								TEXT = [[
سلام!
مدیر این گروه درخاست ربات ضد لینک کرده بود!
اکنون من به این گروه پیوستم!
ابتدا شماره به اشتراک گذاشته شده در بالا را سیو کنید!
سپس من را ادمین گروه کنید تا فعالیتم رو شروع کنم!
برای دردن دستورات ربات از دستور `help` و برای انجام تنظیمات از دستور `settings` استفاده کنید!
اگه ربات مشکلی داشت به @Reload\_LifeBot مراجعه کنید !
]]
								cli.sendText(chat_id, d2.id_, 0, 1, nil, TEXT, 1, 'md', function (a, d3)
								end, nil)
							end, nil)
						end, nil)
					end
				end
				Expire = tonumber((GP.ExpireTime or os.time()))
				if Expire < os.time()  then 
					cli.sendText(chat_id, 0, 0, 1, nil, "شارژ گروه به اتمام رسید !", 1, 'md')
				else 
					if Group.Welcome then
						local text = Group.Welcome 
						if msg.content_.ID == "MessageChatJoinByLink"  then 
						http = require "socket.http"
						J = http.request("https://api.reloadlife.me/".._Config.APITOKEN.."time?timezone=iran")
						print(J)
						JDATA = JSON.decode(J)
						VarDump(JDATA)
							local TEXT = text:gsub("{gpname}", (Group.Title) or "")
							TEXT = TEXT:gsub("{username}", (redis:hget(msg.sender_user_id_, "username") or ""))
							TEXT = TEXT:gsub("{name}", (redis:hget(msg.sender_user_id_, "name") or ""))
							TEXT = TEXT:gsub("{time}", JDATA.result.time)
							TEXT = TEXT:gsub("{date}", JDATA.result.date)
							cli.sendText(chat_id, 0, 0, 1, nil, TEXT, 1, 'md')
						end
						if msg.content_.ID == "MessageChatAddMembers"  then 
						http = require "socket.http"
						J = http.request("https://api.reloadlife.me/".._Config.APITOKEN.."time?timezone=iran")
						print(J)
						JDATA = JSON.decode(J)
						VarDump(JDATA)
							local TEXT = text:gsub("{gpname}", (Group.Title) or "")
							TEXT = TEXT:gsub("{username}", (redis:hget(msg.content_.members_[0].id_, "username") or ""))
							TEXT = TEXT:gsub("{name}", (redis:hget(msg.content_.members_[0].id_, "name") or ""))
							TEXT = TEXT:gsub("{time}", JDATA.result.time)
							TEXT = TEXT:gsub("{date}", JDATA.result.date)
							cli.sendText(chat_id, 0, 0, 1, nil, TEXT, 1, 'md')
						end
					end
					if not isMod(user_id, chat_id) then
						for k,v in pairs(redis:smembers('Filterlist'..chat_id)) do
							if TEXT:find(v) then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if redis:sismember('Mutelist'..chat_id, user_id) then
							if not isMod(user_id, chat_id) then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if tonumber(Group.MuteAll or 0) >= os.time() then
							cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
						end
						if Group.Link == 'DEL' then
							if TEXT:lower():match('t.me/') or TEXT:lower():match('telegram.me/') or TEXT:lower():match('telegram.dog/') or TEXT:lower():match('telegra.ph/') then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						-----------------------------------
						if Group.Atsign == 'DEL' then
							if TEXT:match('@')  then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Hashtag == 'DEL' then
							if TEXT:lower():match('#')  then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.English == 'DEL' then
							if TEXT:lower():match('[a-z]')  then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Persion == 'DEL' then
							if TEXT:lower():match('[\216-\219][\128-\191]')  then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Edit == 'DEL' then
							if msg.edit_date_ ~= 0 then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						-----------------------------------------
						if Group.Photo == 'DEL' then
							if msg.content_.ID == 'MessagePhoto' or msg.content_.photo_ then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Video == 'DEL' then
							if msg.content_.ID == 'MessageVideo' then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.ShareNumber == 'DEL' then
							if msg.content_.ID == "MessageContact" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Music == 'DEL' then
							if msg.content_.ID == "MessageAudio" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Voice == 'DEL' then
							if msg.content_.ID == "MessageVoice" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Location == 'DEL' then
							if msg.content_.ID == "MessageLocation" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Animation == 'DEL' then
							if msg.content_.ID == "MessageAnimation" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Sticker == 'DEL' then
							if msg.content_.ID == "MessageSticker" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Game == 'DEL' then
							if msg.content_.ID == "MessageGame" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Inline == 'DEL' then
							if msg.via_bot_user_id_ ~= 0 then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Keyboard == 'DEL' then
							if msg.reply_markup_ then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.File == 'DEL' then
							if msg.content_.ID == "MessageDocument" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Media == 'DEL' then
							if msg.content_.ID ~= "MessageText" then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Webpage == 'DEL' then
							if msg.content_.web_page_ then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.Forward == 'DEL' then
							if msg.forward_info_ then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						local function CheckFlood(msg) 
							chat_id = (msg.chat_id_ or msg.sender_user_id_)
							user_id = msg.sender_user_id_
							msg_id = msg.id_ 
							reply_id = msg.reply_to_message_id_
							settings = redis:hgetall(msg.chat_id_)
							if msg.content_.ID ~= "MessageChatAddMembers" then
								if not isMod(msg.sender_user_id_, msg.chat_id_) then
									hash = 'user:'..user_id..':'..chat_id..':fldcount'
									redis:incr(hash)
									if redis:get('fld:'..chat_id..':u:'..user_id) == 'ss' then
										if settings.Flood ~= 'OK' then
											if tonumber(redis:get(hash)) > tonumber((settings.FloodC or 5)) then
												redis:set(hash, 0)
												cli.changeChatMemberStatus(chat_id, user_id, 'Kicked')
												text = getUserInfo(user_id)..Language(chat_id, ' Kicked Out!\nUser was Spamming!')
												cli.sendMention(chat_id, user_id, msg_id, text, 0, UTF8.len(getUserInfo(user_id)))
											else
												redis:incrby(hash, 1)
											end
										end
									else
										redis:set(hash, 0)
										redis:setex('fld:'..chat_id..':u:'..user_id, (settings.FloodT or 2), 'ss')
									end
								end
							end
						end
						CheckFlood(msg)
						if Group.LongCharr == 'DEL' then
							if UTF8.len(TEXT) > (tonumber(Group.LongCharrC) or 500) then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
						if Group.ShortCharr == 'DEL' then
							if UTF8.len(TEXT) < (tonumber(Group.ShortCharrC) or 2) then
								cli.deleteMessages(msg.chat_id_, {[0] = msg.id_})
							end
						end
					end
				end
			end
	end
	function tdcli_update_callback(data)  
		if data.ID == 'UpdateMessageEdited' then
			cli.getMessage(data.chat_id_, data.message_id_,
				function (A, D) 
					DoMessage(D)
				end,
				nil
			)
		elseif data.ID == 'UpdateNewMessage' then
				if not data.message_.is_post_ then
					msg = data.message_
					DoMessage(msg)
				end
		elseif (data.ID == "UpdateOption" and data.name_ == "my_id") then
       		tdcli_function ({ID="GetChats",offset_order_="9223372036854775807",offset_chat_id_=0,limit_=20},function(A,D) end,nil)
		end
	end